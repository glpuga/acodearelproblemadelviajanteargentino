# CITES Coding Contest 2016

## Problema

Esta carpeta contiene la solución al problema "El Problema del Viajante Argentino". A partir de la consigna era necesario responder:

  1. ¿En qué orden debe recorrer las ciudades el desarrollador de negocios de CITES, saliendo y volviendo a Sunchales?
  2. ¿Cuál es la distancia total recorrida para la estrategia óptima, incluyendo el tramo final de regreso a Sunchales?

## Solución

Obtener el mínimo global (óptimo) de este problema, es decir, el circuito que minimiza la distancia total recorrida para cualquier recorrido cerrado posible, requiere necesariamente (hasta lo mejor de mi conocimiento) realizar una búsqueda intensiva a lo largo y ancho de todo el espacio de posibilidades. 

Debido al tamaño del problema, con 25 ciudades en el circuito, esto es un proceso computacionalmente muy intenso que toma un tiempo de ejecución elevado para recorrer el espacio de posibilidades completo y poder entonces garantizar que se encontró el mínimo global del mismo. 

Por estas razones es poco probable que se pueda proveer de una respuesta definitiva a las cuestiones planteadas a la hora de cierre del concurso. 

Sin embargo, utilizando el algoritmo que se describe a continuación, y luego de casi
3 hs
de procesamiento en un procesador Intel Core i5 de 4 núcleos y frecuencia de 2.90GHz, 
se halló al menos un recorrido que visita las 25 ciudades en el orden

	Sunchales, Paraná, Buenos Aires, Catamarca, Corrientes, Córdoba, Formosa, Jujuy, La Rioja, Mendoza, Neuquén, Río gallegos, Ushuaia, Trelew, Viedma, Santa Rosa, San Luis, San Juan, Santiago del Estero, Tucumán, Salta, Resistencia, Posadas, La Plata, Santa Fe, Sunchales

y para el cual la distancia total recorrida es de 14745.874968 km.

Dado que el barrido aún no está completo, no se puede garantizar que no existan otros recorridos más cortos.


## Metodología

Para atacar este problema se escribió un programa en lenguaje C que examina el espacio de todos los posibles recorridos en que se pueden recorrer las 25 ciudades y con las restricciones que imponen los 5 cortes de ruta, buscando aquel recorrido que logra el mínimo global de distancia recorrida total. 

Para intentar reducir el tamaño del espacio de búsqueda, el algoritmo tiene en cuenta en todo momento la distancia total del mejor recorrido hallado hasta el momento, información que utiliza para descartar regiones del espacio de búsqueda que necesariamente darán lugar a recorridos más largos.

Además, para hacer un uso más efectivo de los recurso disponibles, el algoritmo examina el espacio de búsqueda utilizando múltiples hilos de ejecución simultánea, de forma de explotar todos los núcleos disponibles en el procesador. 

El algoritmo también se diseñó de forma tal de particionar el espacio de búsqueda de un modo tal que se minimiza la necesidad de interacción entre hilos de ejecución, para eliminar así cualquier problema de rendimiento provocado por la contención en el acceso a datos compartidos. Los únicos accesos a variables compartidas ocurren cuando alguno de los hilos encuentra un nuevo mínimo de distancia total, lo cual es altamente infrecuente luego de los primeros segundos de ejecución.

Para maximizar la poda del espacio de búsqueda y reducir el tiempo de ejecución total, antes de comenzar el proceso de búsqueda intensiva se utiliza un algoritmo heurístico simple para tratar de encontrar rápidamente un recorrido por todas las ciudades que tenga un valor bajo de distancia total recorrida. La heurística utilizada arma un recorrido donde desde cada ciudad se va siempre a la siguiente ciudad más cercana, provisto que esta no haya sido ya incluida en el recorrido y que el camino a la misma no se encuentre cortado.

Por simplicidad los cortes de ruta se modelan como tramos de distancia infinita (en la práctica, muy grandes), tales que recorrerlos provoca automáticamente la poda de todos los recorridos que pasan por ellos.

## Cómo ejecutar la Solución

En la carpeta "build" de los archivos adjuntos se encuentra el Makefile que se debe usar para compilar el programa.

	# make clean
	# make all

Una vez compilado, el programa se puede ejecutar desde esa misma carpeta con:

	# ./elproblemadelviajanteargentino


## Repositorio Git de la solución

Todos los archivos de la solución pueden hallarse en

https://gitlab.com/glpuga/acodearelproblemadelviajanteargentino.git









