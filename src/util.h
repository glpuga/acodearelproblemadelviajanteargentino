/*
 * util.h
 *
 *  Created on: Apr 10, 2016
 *      Author: glpuga
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <stdio.h>
#include <stdlib.h>


#define FATAL_FAIL(errortxt)   { fprintf(stderr, "Error fatal: %s (%s, linea %d)\n", errortxt, __FILE__, __LINE__); exit(1); }

#define ASSERT(cond, errortxt) { if (!(cond)) FATAL_FAIL(errortxt); }

#endif /* UTIL_H_ */
