#include <math.h>
#include "citydata.h"
#include "util.h"

#define CITY_PAIR_IS(id1, id2, codeA, codeB)  ( ((id1 == codeA) && (id2 == codeB)) || ((id1 == codeB) && (id2 == codeA)) )

/* UN-MILLÓN-DE-DÓLARES....  */
#define INFINITE_DISTANCE  1000000;


cityDataType citiesTable[] = {
		{cityIdSunchales,   +000.0,  +000.0, "Sunchales"},
		{cityIdBuenosAires, +355.8,  -405.9,	"Buenos Aires"},
		{cityIdCatamarca,   -467.0,  +274.3,	"Catamarca"},
		{cityIdCorrientes,  +305.8,  +383.6,	"Corrientes"},
		{cityIdCordoba,     -291.0,   -53.7,	"Córdoba"},
		{cityIdFormosa,     +378.1,  +528.2,	"Formosa"},
		{cityIdJujuy,       -415.1,  +750.6,	"Jujuy"},
		{cityIdLaPlata,     +402.2,  -444.8,	"La Plata"},
		{cityIdLaRioja,     -587.5,  +170.5,	"La Rioja"},
		{cityIdMendoza,     -808.0,  -216.8,	"Mendoza"},
		{cityIdNeuquen,     -719.1,  -891.4,	"Neuquén"},
		{cityIdParana,      +116.8,   -89.0,	"Paraná"},
		{cityIdPosadas,     +632.0,  +396.6,	"Posadas"},
		{cityIdResistencia, +287.3,  +387.3,	"Resistencia"},
		{cityIdRioGallegos, -852.5, -2301.7, "Río gallegos"},
		{cityIdSalta,       -426.2,  +683.8,	"Salta"},
		{cityIdSanJuan,     -774.7,   -66.7,	"San Juan"},
		{cityIdSanLuis,     -531.9,  -259.5,	"San Luis"},
		{cityIdSantaFe,      +96.4,   -77.8,	"Santa Fe"},
		{cityIdSantaRosa,   -302.1,  -632.0,	"Santa Rosa"},
		{cityIdSanDelEstero,-300.2,  +350.3,	"Santiago del Estero"},
		{cityIdTrelew,      -415.1, -1369.6, "Trelew"},
		{cityIdTucuman,     -405.9,  +457.8,	"Tucumán"},
		{cityIdUshuaia,     -748.7, -2653.9, "Ushuaia"},
		{cityIdViedma,      -159.4, -1097.1, "Viedma"}
};

void cityMaskAddCity(citiesMaskType *mask, cityIdType id)
{
	citiesMaskType newBit;

	VALIDATE_CITY_ID(id);

	newBit = (1 << id);

	ASSERT(((*mask & newBit) == 0), "El bit de la máscara ya estaba seteado!");

	*mask = *mask + (1 << id); /* el assert de arriba garantiza que esto no tenga acarreos */
}

void cityMaskRemoveCity(citiesMaskType *mask, cityIdType id)
{
	citiesMaskType newBit;

	VALIDATE_CITY_ID(id);

	newBit = (1 << id);

	ASSERT(((*mask & newBit) != 0), "El bit de la máscara no estaba seteado!");

	*mask = *mask - (1 << id); /* el assert de arriba garantiza que esto no tenga acarreos */
}

int32_t cityMaskCityIsSet(citiesMaskType *mask, cityIdType id)
{
	citiesMaskType newBit;

	newBit = (1 << id);

	if ((*mask & newBit) != 0)
	{
		return 1;
	}

	return 0;
}

const char* cityName(cityIdType id)
{
	VALIDATE_CITY_ID(id);

	return citiesTable[id].name;
}

double cityCoordX(cityIdType id)
{
	VALIDATE_CITY_ID(id);

	return citiesTable[id].coordX;
}

double cityCoordY(cityIdType id)
{
	VALIDATE_CITY_ID(id);

	return citiesTable[id].coordY;
}

double cityCalculateDistance(cityIdType city1, cityIdType city2)
{
	double X1, X2, Y1, Y2;
	double distance;

	VALIDATE_CITY_ID(city1);
	VALIDATE_CITY_ID(city2);

	/* Si las ciudades tienen un piquete en la ruta que las une, retorno una distancia infinita,
	 * o lo ques es lo mismo muuuuy grande.
	 *
	 * Rutas problemáticas:
	 * - Jujuy-Salta
	 * - Resistencia-Corrientes
	 * - Paraná-Santa Fe
	 * - Buenos Aires-La Plata
	 * - Mendoza-San Juan
	 * */

	if (CITY_PAIR_IS(city1, city2, cityIdJujuy, cityIdSalta))
	{
		return INFINITE_DISTANCE;
	}

	if (CITY_PAIR_IS(city1, city2, cityIdResistencia, cityIdCorrientes))
	{
		return INFINITE_DISTANCE;
	}

	if (CITY_PAIR_IS(city1, city2, cityIdParana, cityIdSantaFe))
	{
		return INFINITE_DISTANCE;
	}

	if (CITY_PAIR_IS(city1, city2, cityIdBuenosAires, cityIdLaPlata))
	{
		return INFINITE_DISTANCE;
	}

	if (CITY_PAIR_IS(city1, city2, cityIdMendoza, cityIdSanJuan))
	{
		return INFINITE_DISTANCE;
	}

	/* En cualquier otro caso retorno la distancia lineal entre ambas */

	X1 = cityCoordX(city1);
	Y1 = cityCoordY(city1);

	X2 = cityCoordX(city2);
	Y2 = cityCoordY(city2);

	distance = sqrt((X1 - X2) * (X1 - X2) + (Y1 - Y2) * (Y1 - Y2));

	return distance;
}
