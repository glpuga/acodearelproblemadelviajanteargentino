
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <pthread.h>
#include "citydata.h"

#define THREADS_ON 1

/*
 * Estructura de datos que contiene la información de un camino, ya sea total o parcial
 * */
typedef struct {

	cityIdType path[CITY_COUNT - 1]; /* Sunchales no cuenta */

	int32_t citiesVisited;

	double accumulatedDistance;

	citiesMaskType citiesMask;

} pathFinderSolutionType;

/*
 * Estructura de datos usada para almacenar el estado de la búsqueda de un thread
 * */
typedef struct {

	/* Es mejor camino global, de todos los threads, actualizado a la última vez que pregunté... */
	pathFinderSolutionType currentBestPath;

	/* Mi estructura propia donde voy construyendo caminos hasta hallar algo mejor que el mejor global */
	pathFinderSolutionType testPath;

} pathFinderThreadState;

/* *****************
 *
 * Datos protegidos por sincronización
 *
 * */

pthread_mutex_t pathFinderSynchronizationMutex;

/* *****************
 *
 * Datos protegidos por sincronización
 *
 * */

pathFinderSolutionType currentBestPath;

int32_t currentBestPathVersion;

int32_t activeThreadCounter;

/* *****************
 *
 *  Funciones auxiliares
 *
 * */


void pathFinderInitPath(pathFinderSolutionType *path)
{
	int32_t i;

	path->accumulatedDistance = 0.0;

	for (i = 0; i < CITY_COUNT - 1; i++)
	{
		path->path[i] = cityIdInvalid;
	}

	path->citiesVisited = 0;

	path->citiesMask = 0;
}

void pathFinderUnprotectedPrintCurrentBestPath()
{
	int32_t i;

	printf("Versión: %d\n", currentBestPathVersion);

	printf("Camino: %s, ", cityName(cityIdSunchales));
	for (i = 0; i < CITY_COUNT - 1; i++)
	{
		printf("%s, ", cityName(currentBestPath.path[i]));
	}
	printf("%s\n", cityName(cityIdSunchales));

	printf("Distancia total: %f\n", currentBestPath.accumulatedDistance);
}

/* *****************
 *
 *  Funciones thread safe
 *
 * */

void pathFinderGetMutex()
{
#if (THREADS_ON == 1)

	pthread_mutex_lock(&pathFinderSynchronizationMutex);

#endif
}

void pathFinderReleaseMutex()
{
#if (THREADS_ON == 1)

	pthread_mutex_unlock(&pathFinderSynchronizationMutex);

#endif
}

void pathFinderSafeIncreaseActiveThreadCounter()
{
	/* Entro en la sección crítica */
	pathFinderGetMutex();

	ASSERT(activeThreadCounter >= 0, "Valor del contador de threads inválido");

	/* Entro en la sección crítica */
	activeThreadCounter++;

	/* Salgo de la sección crítica */
	pathFinderReleaseMutex();
}

void pathFinderSafeDecreaseActiveThreadCounter()
{
	/* Entro en la sección crítica */
	pathFinderGetMutex();

	/* Entro en la sección crítica */
	activeThreadCounter--;

	ASSERT(activeThreadCounter >= 0, "Valor del contador de threads inválido");

	/* Salgo de la sección crítica */
	pathFinderReleaseMutex();
}

int32_t pathFinderSafeGetCurrentActiveThreadCounter()
{
	int32_t value;

	/* Entro en la sección crítica */
	pathFinderGetMutex();

	/* Entro en la sección crítica */
	value = activeThreadCounter;

	/* Salgo de la sección crítica */
	pathFinderReleaseMutex();

	return value;
}


void pathFinderSafePrintCurrentBestPath()
{

	/* Entro en la sección crítica */
	pathFinderGetMutex();

	pathFinderUnprotectedPrintCurrentBestPath();

	/* Salgo de la sección crítica */
	pathFinderReleaseMutex();
}


void pathFinderSafeSetNewBestPath(pathFinderSolutionType *newBestPathPtr)
{
	int32_t i;

	/* Hago una verificación elemental de la nueva mejor solución para no aceptar pescado podrido. */

	ASSERT(newBestPathPtr->citiesVisited == CITY_COUNT - 1, "La nueva mejor solución no me gusta!");

	for (i = 0; i < CITY_COUNT - 1; i++)
	{
		ASSERT(newBestPathPtr->path[i] != cityIdInvalid - 1, "La nueva mejor solución no me gusta!");
	}

	/* Entro en la sección crítica */
	pathFinderGetMutex();

	/* Ya dentro de la sección crítica verifico que efectivamente esta nueva mejor solución siga siendo mejor solución,
	 * porque otro hilo pudo haber encontrado una solución mejor que la más recientemente conocida por el hilo que envía
	 * esta candidata.
	 */

	if (newBestPathPtr->accumulatedDistance < currentBestPath.accumulatedDistance)
	{
		/* Es mejor, así que es el nuevo mejor circuito */
		currentBestPath = *newBestPathPtr;

		/* Incremento el número de versión */
		currentBestPathVersion++;

		printf("\n");
		pathFinderUnprotectedPrintCurrentBestPath();

	} else {

		/* No hago nada. El hilo que llama a esta función está obligado a actualizar su copia del nuevo mejor camino
		 * después de haber llamado a esta función, así que todo se va a arreglar solo. */
	}

	/* Salgo de la sección crítica */
	pathFinderReleaseMutex();
}

void pathFinderSafeGetCurrentBestPath(pathFinderSolutionType *newBestPathPtr)
{
	/* Entro en la sección crítica */
	pathFinderGetMutex();

	/* Copio el mejor circuito más actual a la estructura del hilo que llama */
	*newBestPathPtr = currentBestPath;

	/* Salgo de la sección crítica */
	pathFinderReleaseMutex();
}

void pathFinderTryNext(pathFinderThreadState *threadStatePtr, cityIdType nextCityToTry)
{
	cityIdType lastCityInPath;
	cityIdType cityIndex;
	double newDistanceValue, oldDistanceValue;

	/*
	 * Verifico si agregando ciudad al camino actual el camino se vuelve más largo que
	 * el mejor conocido. Si es así puedo descartar este camino y podar la rama de
	 * búsqueda completa.
	 * */
	lastCityInPath = threadStatePtr->testPath.path[threadStatePtr->testPath.citiesVisited - 1];

	newDistanceValue = threadStatePtr->testPath.accumulatedDistance + cityCalculateDistance(lastCityInPath, nextCityToTry);

	if (newDistanceValue > threadStatePtr->currentBestPath.accumulatedDistance)
	{
		/*
		 * Este camino debe podarse, nunca va a generar una solución mejor que la mejor solución ya conocida.
		 *
		 * Todavía no hice ningún cambio en el el estado del hilo, así que no necesito deshacer cambios.
		 * */
		return;
	}

	/*
	 * Este camino tiene que explorarse. Agrego la ciudad al circuito. Cuando abandone el análisis de
	 * este nodo tengo que poder deshacer estos cambios...
	 */
	threadStatePtr->testPath.path[threadStatePtr->testPath.citiesVisited] = nextCityToTry;

	oldDistanceValue = threadStatePtr->testPath.accumulatedDistance; /* Guardo este valor para no tene que recalcularlo luego */
	threadStatePtr->testPath.accumulatedDistance = newDistanceValue;

	threadStatePtr->testPath.citiesVisited++;

	cityMaskAddCity(&threadStatePtr->testPath.citiesMask, nextCityToTry);

	/* Si al agregar esta ciudad ya están todas las ciudades en el circuito, entonces
	 * comparo con la mejor ruta conocida globalmente */
	if (threadStatePtr->testPath.citiesVisited == (CITY_COUNT - 1))
	{

		/* Ya están todas la ciudades en el circuito. Sumo la distancia
		 * del retorno desde la última ciudad hasta sunchales y comparo
		 * con la solución global */
		threadStatePtr->testPath.accumulatedDistance += cityCalculateDistance(nextCityToTry, cityIdSunchales);

		if (threadStatePtr->testPath.accumulatedDistance < threadStatePtr->currentBestPath.accumulatedDistance)
		{
			/* Tenemos un nuevo mejor camino! */
			pathFinderSafeSetNewBestPath(&threadStatePtr->testPath);

			/* Vuelvo a preguntar cual es el mejor camino porque mientras
			 * yo buscaba otro hilo pudo haber encontrado otro camino todavía
			 * mejor que este que yo encontré, en cuyo caso el SET anterior
			 * no tiene efecto. Ver rutina correspondiente.
			 */
			pathFinderSafeGetCurrentBestPath(&threadStatePtr->currentBestPath);
		}

	} else {

		/*
		 * No puedo decir nada, estoy un un nodo intermedio del árbol de búsqueda y no pude podar,
		 * así que sigo subiendo por las ramas.
		 * */

		for (cityIndex = cityIdFirstValue; cityIndex <= cityIdLastValue; cityIndex++)
		{
			/* Salteo las ciudades que ya están en el camino parcial */
			if (cityMaskCityIsSet(&threadStatePtr->testPath.citiesMask, cityIndex) != 0)
			{
				continue;
			}

			/* Las demás las examino recursivamente */
			pathFinderTryNext(threadStatePtr, cityIndex);
		}
	}

	/*
	 * Deshago los cambios al estado del camino parcial.
	 */
	cityMaskRemoveCity(&threadStatePtr->testPath.citiesMask, nextCityToTry);

	threadStatePtr->testPath.citiesVisited--;

	threadStatePtr->testPath.accumulatedDistance = oldDistanceValue;

	threadStatePtr->testPath.path[threadStatePtr->testPath.citiesVisited] = cityIdInvalid;
}

void pathFinderSafeSearchStartingIn(cityIdType firstCityInPath)
{
	pathFinderThreadState *threadStatePtr;
	cityIdType nextCityToTry;

	VALIDATE_CITY_ID(firstCityInPath);

	/* Creo la estructura de datos propia que va a almacenar el estado de este hilo.
	 * Estoy asumiendo que malloc es thread_safe...  */
	threadStatePtr = (pathFinderThreadState *)malloc(sizeof(pathFinderThreadState));

	/* Determino cuál es el mejor camino global actual */
	pathFinderSafeGetCurrentBestPath(&threadStatePtr->currentBestPath);

	/* Inicializo la estructura que va a contener el camino parcial */
	pathFinderInitPath(&threadStatePtr->testPath);

	/* Le cargo los datos del primer punto en el circuito */
	threadStatePtr->testPath.path[0]       = firstCityInPath;
	threadStatePtr->testPath.citiesVisited = 1;

	cityMaskAddCity(&threadStatePtr->testPath.citiesMask, cityIdSunchales);
	cityMaskAddCity(&threadStatePtr->testPath.citiesMask, firstCityInPath);

	/*
	 * Ahora empieza el trabajo en serio...
	 * */

	for (nextCityToTry = cityIdFirstValue; nextCityToTry <= cityIdLastValue; nextCityToTry++)
	{
		/* Si la ciudad ya está en la ruta parcial entonces no la examino */
		if (cityMaskCityIsSet(&threadStatePtr->testPath.citiesMask, nextCityToTry) != 0)
		{
			continue;
		}

		/* En cualquier otro caso examino esa parte del espacio de posibilidades */
		pathFinderTryNext(threadStatePtr, nextCityToTry);
	}

	/* Libero la memoria y salgo */
	free((void *)threadStatePtr);
}


/* *****************
 *
 *  Funciones de control
 *
 * */

void pathFinderInitModule()
{
	pathFinderSolutionType defaultPath;
	cityIdType previousCity, currentCity;

	int32_t i;

	/* Construyo un camino por defecto que pase por todas las ciudades,
	 * para tener para comparar cuando empiece a realizar la búsqueda. */

	previousCity = cityIdSunchales;

	pathFinderInitPath(&defaultPath);

	for (i = 0; i < CITY_COUNT - 1; i++)
	{
		currentCity = previousCity + 1;

		defaultPath.path[i] = currentCity;
		defaultPath.accumulatedDistance += cityCalculateDistance(previousCity, currentCity);

		cityMaskAddCity(&defaultPath.citiesMask, currentCity);

		previousCity = currentCity;
	}

	defaultPath.accumulatedDistance += cityCalculateDistance(previousCity, cityIdSunchales);

	defaultPath.citiesVisited = CITY_COUNT - 1;

	/* El mejor camino conocido hasta ahora es este */
	currentBestPath = defaultPath;

	/* No hay artificial o no, es la primera versión */
	currentBestPathVersion = 1;

	/* Creo el mutex de exclusión a datos compartidos */
	pthread_mutex_init(&pathFinderSynchronizationMutex, NULL);
}

void pathFinderFindApproximateSolution()
{
	pathFinderSolutionType approximateSolution;
	pathFinderSolutionType currentBestPath;
	cityIdType previousCity, cityIndex;
	double distance;

	cityIdType minDistanceCity;
	double minDistanceValue;
	int32_t minDistanceIsInitialized;

	int32_t i;

	/* Construyo un camino por defecto que pase por todas las ciudades,
	 * para tener para comparar cuando empiece a realizar la búsqueda. */

	previousCity = cityIdSunchales;

	pathFinderInitPath(&approximateSolution);

	cityMaskAddCity(&approximateSolution.citiesMask, cityIdSunchales);

	for (i = 0; i < CITY_COUNT - 1; i++)
	{
		/* Busco a cada paso la ciudad más cercana a la anterior. En el último paso, el regreso a Sunchales
		 * puede ser arbitrariamente largo, pero en principio el método puede dar un punto de arranque razonable
		 * que el circuito por defecto de la inicialización. */

		minDistanceIsInitialized = 0;
		minDistanceValue = 1.0e6;
		minDistanceCity  = cityIdInvalid;

		for (cityIndex = cityIdFirstValue; cityIndex <= cityIdLastValue; cityIndex++)
		{
			/* Si la ciudad ya está en la ruta parcial la paso por alto */
			if (cityMaskCityIsSet(&approximateSolution.citiesMask, cityIndex) != 0)
			{
				continue;
			}

			distance = cityCalculateDistance(previousCity, cityIndex);

			/* Si todavía no se inicializó un mínimo, o si la distancia a esta ciudad es más chica que el mínimo conocido, lo actualizo */
			if ((minDistanceIsInitialized == 0) || (distance < minDistanceValue))
			{
				minDistanceCity          = cityIndex;
				minDistanceValue         = distance;
				minDistanceIsInitialized = 1;
			}
		}

		/* Necesariamente debería haber encontrado una al menos una ciudad para saltar a continuación */
		ASSERT(minDistanceIsInitialized == 1, "Error en el algortimo de aproximación ávida.");

		/* Me desplazo a la ciudad recién hallada, que es la más cercana a la anterior que
		 * todavía no está en el camino parcial. */

		approximateSolution.path[i]              = minDistanceCity;
		approximateSolution.accumulatedDistance += minDistanceValue;

		cityMaskAddCity(&approximateSolution.citiesMask, minDistanceCity);

		approximateSolution.citiesVisited++;

		previousCity = minDistanceCity;
	}

	/* Por último sumo la distancia del retorno a Sunchales */
	approximateSolution.accumulatedDistance += cityCalculateDistance(previousCity, cityIdSunchales);


	/* Veo si la solución aproximada es mejor que la por defecto */
	pathFinderSafeGetCurrentBestPath(&currentBestPath);

	if (approximateSolution.accumulatedDistance < currentBestPath.accumulatedDistance)
	{
		printf("Heurísticamente se encontró un mejor punto de partida que la ruta por defecto!\n");

		pathFinderSafeSetNewBestPath(&approximateSolution);
	} else {
		printf("El método heurístico no logró ninguna mejora respecto de la ruta por defecto, por lo que su solución será ignorada.\n");
	}
}


void  *pathFinderDoSearchThread(void *ptr)
{
	cityIdType cityId;

	cityId = *((cityIdType*)ptr);

	/* En cualquier otro caso examino esa parte del espacio de posibilidades */
	pathFinderSafeSearchStartingIn(cityId);

	/* Indico que el hilo terminó su ejecución */
	pathFinderSafeDecreaseActiveThreadCounter();

	/* Termino el hilo */
	pthread_exit(NULL);

	return NULL;
}


void pathFinderDoPathSearch()
{
	pthread_t threadId;
	cityIdType cityIndex;
	cityIdType *cityIdPtr;

	for (cityIndex = cityIdFirstValue; cityIndex <= cityIdLastValue; cityIndex++)
	{
		/* Sunchales no cuenta */
		if (cityIndex == cityIdSunchales)
		{
			continue;
		}

#if (THREADS_ON == 1)

		/*
		 * Con threads creo un thread para cada subconjunto de caminos que comienza la búsqueda con una ciudad diferente.
		 * */

		pathFinderSafeIncreaseActiveThreadCounter();

		cityIdPtr = (void*) malloc(sizeof(cityIdType));

		*cityIdPtr = cityIndex;

		pthread_create (&threadId, NULL, &pathFinderDoSearchThread, (void*)cityIdPtr);

#else
		/*
		 * Sin threads ejecuto serialmente todas las búsquedas serialmente
		 * */

		/* En cualquier otro caso examino esa parte del espacio de posibilidades */
		pathFinderSafeSearchStartingIn(cityIndex);
#endif
	}
}

void pathFinderWaitForThreadsToFinish()
{
	int32_t currentActiveThreadCounter = 0;

#if (THREADS_ON == 1)

	do {

		sleep(1);

		currentActiveThreadCounter = pathFinderSafeGetCurrentActiveThreadCounter();

	} while (currentActiveThreadCounter > 0);

#endif

}

/* *****************
 *
 *  Funciones de interfaz
 *
 * */

void pathFinderMain()
{

	pathFinderInitModule();

	printf("\n");
	printf("\n");
	printf("Módulo inicializado\n");
	printf("\n");
	printf("Se cargó la versión inicial del recorrido...\n");
	pathFinderSafePrintCurrentBestPath();
	printf("\n");

	/* Busco a ver si encuentro una solución aproximada mejor, para
	 * permitor un nivel de poda más profundo durante la búsqueda. */
	pathFinderFindApproximateSolution();

	printf("\n");
	printf("----\n");
	printf("\n");

	pathFinderDoPathSearch();

	pathFinderWaitForThreadsToFinish();

	printf("----\n");
	printf("\n");
	printf("Búsqueda finalizada\n");
	printf("\n");
	pathFinderSafePrintCurrentBestPath();
	printf("\n");
}







