
#ifndef CITYDATA_H_
#define CITYDATA_H_

#include <stdint.h>
#include "util.h"

#define DEBUG_MODE 0

#define VALIDATE_CITY_ID(id) { ASSERT((id >= cityIdFirstValue) && (id <= cityIdLastValue), "Identificador inválido."); }

#if (DEBUG_MODE == 1)

#define CITY_COUNT 15

#else

#define CITY_COUNT 25

#endif

typedef enum {
	cityIdInvalid     = -1,
	cityIdFirstValue  = 0,
	/* ----------------- */
	cityIdSunchales   = 0,
	cityIdBuenosAires = 1,
	cityIdCatamarca   = 2,
	cityIdCorrientes  = 3,
	cityIdCordoba     = 4,
	cityIdFormosa     = 5,
	cityIdJujuy       = 6,
	cityIdLaPlata     = 7,
	cityIdLaRioja     = 8,
	cityIdMendoza     = 9,
	cityIdNeuquen     = 10,
	cityIdParana      = 11,
	cityIdPosadas     = 12,
	cityIdResistencia = 13,
	cityIdRioGallegos = 14,
	cityIdSalta       = 15,
	cityIdSanJuan     = 16,
	cityIdSanLuis     = 17,
	cityIdSantaFe     = 18,
	cityIdSantaRosa   = 19,
	cityIdSanDelEstero= 20,
	cityIdTrelew      = 21,
	cityIdTucuman     = 22,
	cityIdUshuaia     = 23,
	cityIdViedma      = 24,
	/* ----------------- */
	cityIdLastValue   = (CITY_COUNT - 1)

} cityIdType;

typedef struct {

	cityIdType id;

	double coordX;
	double coordY;

	const char *name;

} cityDataType;

typedef uint32_t citiesMaskType;

void cityMaskAddCity(citiesMaskType *mask, cityIdType id);

void cityMaskRemoveCity(citiesMaskType *mask, cityIdType id);

int32_t cityMaskCityIsSet(citiesMaskType *mask, cityIdType id);

const char* cityName(cityIdType id);

double cityCoordX(cityIdType id);

double cityCoordY(cityIdType id);

double cityCalculateDistance(cityIdType city1, cityIdType city2);


#endif /* CITYDATA_H_ */
